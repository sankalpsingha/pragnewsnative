import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";

// Importing all the screens for the application


import NewsScreen from "./screens/NewsScreen";
import SingleNewsScreen from "./screens/SingleNewsScreen";
import EntertainmentNews from "./screens/EntertainmentNews";
import RegionalNews from "./screens/RegionalNews";

// import GeetNews from "./screens/GeetNews";
import SportsNews from "./screens/SportsNews";
// import WorldNews from "./screens/WorldNews";
import NationalNews from "./screens/NationalNews";


import React from "react";
import { TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import HeaderLogo from "../src/misc/HeaderLogo";

const MyDrawerNavigator = createDrawerNavigator({ 
  NewsList: {
    	screen: NewsScreen,
    		navigationOptions: {
    			title: "All News",
    		},
    	},
  Entertainment: {
    screen: EntertainmentNews,
  },
  Regional: {
    screen: RegionalNews,
  },
	// Geet: {
	// 	screen: GeetNews,
	// },
	National: {
		screen: NationalNews,
	},
	// World: {
	// 	screen: WorldNews,
	// },
	Sports: {
		screen: SportsNews,
	},
});

const AppNavigator = 
    createStackNavigator({
		Home: {
			screen: MyDrawerNavigator,
			navigationOptions: ({ navigation }) => ({
				title: "Prag News",
				headerTitle: <HeaderLogo />,
				headerTintColor: "#000",
				headerStyle: {
					backgroundColor: "#2B2A2A",
				},
				headerLeft: <MenuButton navigate={navigation} />,
			}),
		},
    	NewsList: {
    		screen: NewsScreen,
    		navigationOptions: {
    			title: "Prag News",
    		},
    	},
    	SingleNews: {
				screen: SingleNewsScreen,
				navigationOptions: ({ navigation }) => ({
					title: navigation.state.params && navigation.state.params.backgroundColor === "#000" ? "Back" : "",
					headerTransparent: navigation.state.params && navigation.state.params.backgroundColor === "#000" ? false : true,
					headerStyle: {
						backgroundColor: navigation.state.params && navigation.state.params.backgroundColor === "#000" ? "#2B2A2A" : "transparent",
						zIndex: 100,
						elevation: navigation.state.params && navigation.state.params.backgroundColor === "#000" ? 1 : 0,
						shadowOpacity: 0,
						borderBottomWidth: 0,
					},
					headerTintColor: "#fff",
					headerTitleStyle: {
						color: "#fff",
					},
				}),
    	},
    });


const MenuButton = ({ navigate }) => {
	// console.log("vhkcvkd", navigate);
	return (
		// eslint-disable-next-line react-native/no-inline-styles
		<TouchableOpacity activeOpacity={1} onPress={() => { console.log("Button pressed"); navigate.toggleDrawer(); } } >
			<Icon name="bars" size={30} style={{marginLeft: 10, color: "#ffffff"}} />
		</TouchableOpacity>
	);
};

export default createAppContainer(AppNavigator);