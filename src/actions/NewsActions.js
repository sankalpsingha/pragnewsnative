import {
	NEWS_FETCHED_SUCCESS,
	SINGLE_NEWS_FETCHED_SUCCESS,
    LOADING_START,
    MORE_NEWS_FETCHED,
    NEWS_ENTERTAINMENT_FETCHED_SUCCESS,
    NEWS_GEET_FETCHED_SUCCESS, 
    NEWS_REGIONAL_FETCHED_SUCCESS,
    NEWS_NATIONAL_FETCHED_SUCCESS,
    NEWS_SPORTS_FETCHED_SUCCESS,
    NEWS_WORLD_FETCHED_SUCCESS,

} from "./types.js";

import { 
    API_ENDPOINT,
    ENTERTAINMENT_CATEGORY, 
    REGIONAL_CATEGORY,
    GEET_CATEGORY,
    NATIONAL_CATEGORY,
    WORLD_CATEGORY,
    SPORTS_CATEGORY, 
} from "./api";

export const newsFetch = (category) => {
    var category_endpoint = category || "ALL";
    // Start the loader..

    console.log("The category selected in the action : " + category_endpoint);

    switch(category_endpoint) {
        case "ALL": {
        return(dispatch) => {
                console.log(`${API_ENDPOINT}/posts?_embed`);
                fetch(`${API_ENDPOINT}/posts?_embed`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        dispatch({
                            type: NEWS_FETCHED_SUCCESS,
                            payload: data,
                        });
                    });
                });

            };
        }

        case ENTERTAINMENT_CATEGORY: {
             return(dispatch) => {
                console.log(`${API_ENDPOINT}/posts?_embed&categories=${ENTERTAINMENT_CATEGORY}`);

                fetch(`${API_ENDPOINT}/posts?_embed&categories=${ENTERTAINMENT_CATEGORY}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        dispatch({
                            type: NEWS_ENTERTAINMENT_FETCHED_SUCCESS,
                            payload: data,
                        });
                    });
                });

            };
        }

        case GEET_CATEGORY: {
            return(dispatch) => {
               console.log(`${API_ENDPOINT}/posts?_embed&categories=${GEET_CATEGORY}`);

               fetch(`${API_ENDPOINT}/posts?_embed&categories=${GEET_CATEGORY}`, {
                   method: "GET",
                   headers: {
                       Accept: "application/json",
                       "Content-Type": "application/json",
                   },
               }).then(response => {
                   response.json().then(data => {
                       dispatch({
                           type: NEWS_GEET_FETCHED_SUCCESS,
                           payload: data,
                       });
                   });
               });

           };
       }

    case REGIONAL_CATEGORY: {
        return(dispatch) => {
           console.log(`${API_ENDPOINT}/posts?_embed&categories=${REGIONAL_CATEGORY}`);

           fetch(`${API_ENDPOINT}/posts?_embed&categories=${REGIONAL_CATEGORY}`, {
               method: "GET",
               headers: {
                   Accept: "application/json",
                   "Content-Type": "application/json",
               },
           }).then(response => {
               response.json().then(data => {
                   dispatch({
                       type: NEWS_REGIONAL_FETCHED_SUCCESS,
                       payload: data,
                   });
               });
           });

       };
   }

    case NATIONAL_CATEGORY: {
        return(dispatch) => {
            console.log(`${API_ENDPOINT}/posts?_embed&categories=${NATIONAL_CATEGORY}`);

            fetch(`${API_ENDPOINT}/posts?_embed&categories=${NATIONAL_CATEGORY}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }).then(response => {
                response.json().then(data => {
                    dispatch({
                        type: NEWS_NATIONAL_FETCHED_SUCCESS,
                        payload: data,
                    });
                });
            });

        };
    }

    case SPORTS_CATEGORY: {
        return(dispatch) => {
        console.log(`${API_ENDPOINT}/posts?_embed&categories=${SPORTS_CATEGORY}`);

            fetch(`${API_ENDPOINT}/posts?_embed&categories=${SPORTS_CATEGORY}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }).then(response => {
                response.json().then(data => {
                    dispatch({
                        type: NEWS_SPORTS_FETCHED_SUCCESS,
                        payload: data,
                    });
                });
            });

        };
    }

    case WORLD_CATEGORY: {
        return(dispatch) => {
            console.log(`${API_ENDPOINT}/posts?_embed&categories=${WORLD_CATEGORY}`);

            fetch(`${API_ENDPOINT}/posts?_embed&categories=${WORLD_CATEGORY}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            }).then(response => {
                response.json().then(data => {
                    dispatch({
                        type: NEWS_WORLD_FETCHED_SUCCESS,
                        payload: data,
                    });
                });
            });

        };
    }
        default:
            return [];
    }
	

    
};


export const selectNews = (newsID, navigation) => {
    return(dispatch) => {
        dispatch({
			        type: LOADING_START,
		        });
        navigation.navigate("SingleNews");

        fetch(`${API_ENDPOINT}/posts/${newsID}?_embed`, {
            method: "GET",
            headers: {
                 Accept: "application/json",
                "Content-Type": "application/json",
            },
        }).then(response => {
            response.json().then(data => {
                dispatch({
			        type: SINGLE_NEWS_FETCHED_SUCCESS,
                    payload: data,
		        });
            });
        });
    };
};


export const fetchMoreNews = (pageNo,category) => {
    var category_endpoint = category || "ALL";

    switch(category_endpoint) {
        case "ALL": {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts/?_embed&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                         Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.news);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.news.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                news: a,
                            },
                        });
                    });
                });
            };
        }

        case ENTERTAINMENT_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${ENTERTAINMENT_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                         Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.entertainmentNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.entertainmentNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                entertainmentNews: a,
                            },
                        });
                    });
                });
            };
       }

       case REGIONAL_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${REGIONAL_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.regionalNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.regionalNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                regionalNews: a,
                            },
                        });
                    });
                });
            };
        }

        case NATIONAL_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${NATIONAL_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.regionalNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.nationalNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                nationalNews: a,
                            },
                        });
                    });
                });
            };
        }

        case SPORTS_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${SPORTS_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.sportsNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.sportsNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                sportsNews: a,
                            },
                        });
                    });
                });
            };
        }

        case WORLD_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${WORLD_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.worldNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.worldNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                worldNews: a,
                            },
                        });
                    });
                });
            };
        }

        
        case GEET_CATEGORY: {
            return(dispatch, getState) => {
                fetch(`${API_ENDPOINT}/posts?_embed&categories=${GEET_CATEGORY}&page=${pageNo}`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                }).then(response => {
                    response.json().then(data => {
                        console.log(getState().news.geetNews);
                        // eslint-disable-next-line vars-on-top
                        var a = getState().news.geetNews.concat(data);
                        dispatch({
                            type: MORE_NEWS_FETCHED,
                            payload: {
                                geetNews: a,
                            },
                        });
                    });
                });
            };
        }
        default:
        return [];
    }
 
};