import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Dimensions, Text, ImageBackground } from "react-native";
import { connect } from "react-redux";
import HTMLView from "react-native-htmlview";
import LinearGradient from "react-native-linear-gradient";
import he from "he"; // This is the encoder / decoder for the HTML special characters
// import Icon from "react-native-vector-icons/FontAwesome";
import Spinner from "../components/Spinner";

const { WIDTH } = Dimensions.get("window");
const styles = {
	scrollStyle: { 
		padding: 10, 
	},
	bannerImageStyle: {
		width: WIDTH,
		resizeMode: "contain",
		height: 250,
	},
	linearGradient: {
		flex: 1,
		position: "absolute",
		bottom: 0,
		padding: 20,
	},
	titleTextStyle: {
		color: "#ccc",
		textAlign: "center",
		fontWeight: "bold",
	},
	undefinedImageSection: {
		backgroundColor: "#fff",
		padding: 25,
		elevation: 5,
	  },
};

const HTMLStyles = StyleSheet.create({
	// eslint-disable-next-line react-native/no-unused-styles
	p: {
		marginBottom: -60,
	},
});
class SingleNewsScreen extends Component {

	checkForUndefined() {
		if(this.props.single._embedded["wp:featuredmedia"]) {
			return (
				<ImageBackground source={{uri: this.props.single._embedded["wp:featuredmedia"]["0"].source_url}}
							style={styles.bannerImageStyle}>
					<LinearGradient colors={["rgba(0,0,0,0.01)", "rgba(0,0,0,0.3)", "rgba(0,0,0,0.5)", "rgba(0,0,0,0.7)", "rgba(0,0,0,1)"]} style={styles.linearGradient}>
					   <Text style={styles.titleTextStyle}>{he.decode(this.props.single.title.rendered)}</Text>
				    </LinearGradient>
				</ImageBackground>
			);
		}
			return(
				<View style={styles.undefinedImageSection}>
					<Text style={[styles.titleTextStyle, { fontSize: 17, color: "#000" }]}>{he.decode(this.props.single.title.rendered)}</Text>
				</View>
			); 
		
	}
	
	fetchSingle() {
		if(this.props.loading){
			return(
				<Spinner />
			);
		}
		return (
			<View style={{ marginTop: 0 }}>
				
				{this.checkForUndefined()}
				
				<View style={styles.scrollStyle}>
					<HTMLView
						value={this.props.single.content.rendered.replace(/\&nbsp;/g, "")}
						stylesheet={HTMLStyles}
					/>
				</View>
			</View>
		);
	}
	handleScroll(event) {
		if(event.nativeEvent.contentOffset.y >= 200) {
			this.props.navigation.setParams({ backgroundColor: "#000", color: "#fff" });
		} else {
			this.props.navigation.setParams({ backgroundColor: "transparent", color: "#fff" });
		}
	}
	render () {
        // console.log("Single: " + JSON.stringify(this.props.single));

		return (
			<ScrollView onScroll={this.handleScroll.bind(this)}> 
				{this.fetchSingle()}
			</ScrollView>
		);
	}
}

const mapStateToProps = (state) => {
    return ({
		loading: state.news.loading,
        single: state.news.singleNews,
    });
};
export default connect(mapStateToProps)(SingleNewsScreen);

