import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { connect } from "react-redux";
import { newsFetch, fetchMoreNews } from "../actions";
import NewsCard from "../components/NewsCard";
import { WORLD_CATEGORY } from "../actions/api";
import Spinner from "../components/Spinner";
import FilteredNewsHeader from "../components/FilteredNewsHeader";

class WorldNews extends Component {
    constructor(props) {
		super(props);
		this.state = {
			pageNo: 1,
			refreshing: false,
		};
	}
	componentDidMount() {
        console.log("The loaded cat: " + WORLD_CATEGORY);
		this.props.newsFetch(WORLD_CATEGORY);
	}
	renderNewsCard({item}) {
		// console.log(self.props.navigation);
		return(
			<NewsCard item={item} navigationProp={this.props.navigation} />
		);
	}

	loadMoreNews () {
		console.log("Loading more...");
		this.setState({
			pageNo: this.state.pageNo + 1,
		}, () => {
			console.log("Making a remote req...");
			console.log("Here is the updated state :" + this.state.pageNo);
			this.props.fetchMoreNews(this.state.pageNo,WORLD_CATEGORY);
		});
	}

	footerComponentLoader() {
		return(
			// eslint-disable-next-line react-native/no-inline-styles
			<View style={{marginBottom: 20, marginTop: -20}}>
				<Spinner />
			</View>
		);
	}

	refreshedList() {
		console.log("You pulled to refresh...");
		this.props.newsFetch();
	}

	headerComponent() {
		return(
			<FilteredNewsHeader headerText={"World News:"}/>
		);
	}
	renderNewsList() {
		if(this.props.loading) { 
			return(
				<Spinner />
			);
		}

		return(
			<View>
				<FlatList
					data={this.props.news}
					
					renderItem={this.renderNewsCard.bind(this)} 
					keyExtractor={item => item.id.toString()}
					onEndReached= {() => this.loadMoreNews()}
					onEndReachedThreshold={0.4}
					
					ListHeaderComponent = {this.headerComponent}
					ListFooterComponent= {this.footerComponentLoader}

					// Setting up the pull to refresh
					refreshing = {this.state.refreshing}
					onRefresh = {this.refreshedList.bind(this)}
				/>
			</View>
		);
	}

	render () {
		return (
				<View>
					{this.renderNewsList()}
				</View>
		);
	}
}
const mapStateToProps = state => {
	// console.log("The current state: " + JSON.stringify(state));
	return {
		news: state.news.worldNews,
		loading: state.news.loading,
	};
};

export default connect(mapStateToProps, { newsFetch, fetchMoreNews })(WorldNews);
