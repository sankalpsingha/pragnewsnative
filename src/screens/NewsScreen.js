import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { connect } from "react-redux";
import { newsFetch, fetchMoreNews } from "../actions";
import NewsCard from "../components/NewsCard";

import Spinner from "../components/Spinner";
import MyCarousal from "../components/MyCarousal";

var firstFourNews;
var lastPartListNews;
class NewsScreen extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			pageNo: 1,
			refreshing: false,
		};
	}
	componentDidMount() {
		this.props.newsFetch();
	}
	renderNewsCard({item}) {
		// console.log(self.props.navigation);
		return(
			<NewsCard item={item} navigationProp={this.props.navigation} />
		);
	}

	loadMoreNews () {
		console.log("Loading more...");
		this.setState({
			pageNo: this.state.pageNo + 1,
		}, () => {
			// console.log("Making a remote req...");
			// console.log("Here is the updated state git:" + this.state.pageNo);
			this.props.fetchMoreNews(this.state.pageNo);
		});
	}

	footerComponentLoader() {
		return(
			// eslint-disable-next-line react-native/no-inline-styles
			<View style={{marginBottom: 20, marginTop: -20}}>
				<Spinner />
			</View>
		);
	}

	refreshedList() {
		console.log("You pulled to refresh...");
		this.props.newsFetch();
	}

	carousalLoad = () => {
		// console.log("First four", firstFourNews);
		return(
			<MyCarousal navigation={this.props.navigation} items={firstFourNews} />
		);
	}
	renderNewsList() {
		if(this.props.loading) { 
			return(
				<Spinner />
			);
		}

		return(
			<View>
				<FlatList
					data={lastPartListNews}
					
					renderItem={this.renderNewsCard.bind(this)} 
					keyExtractor={item => item.id.toString()}
					onEndReached= {() => this.loadMoreNews()}
					onEndReachedThreshold={0.4}
					
					ListHeaderComponent = {this.carousalLoad}
					ListFooterComponent= {this.footerComponentLoader}

					// Setting up the pull to refresh
					refreshing = {this.state.refreshing}
					onRefresh = {this.refreshedList.bind(this)}
				/>
			</View>
		);
	}

	render () {
		firstFourNews = this.props.news.news.slice(0,5);
		lastPartListNews = this.props.news.news.slice(5);
		
		console.log("Splitted array", this.props.news.news, firstFourNews);
		return (
				<View>
					{this.renderNewsList()}
				</View>
		);
	}
}
const mapStateToProps = state => {
	// console.log("The current state: " + JSON.stringify(state));
	return {
		news: state.news,
		loading: state.news.loading,
	};
};

export default connect(mapStateToProps, { newsFetch, fetchMoreNews })(NewsScreen);
