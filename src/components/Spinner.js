import React from "react";
import { ActivityIndicator, View } from "react-native";

const styles = {
    viewStyle: {
        alignItem: "center",
        flex: 1,
        justifyContent: "center",
        marginTop: 50,
    },
};
const Spinner = () => {
    return(
        <View style={styles.viewStyle}>
           <ActivityIndicator size="large" color="#0000ff" />
        </View>
    );
};

export default Spinner;