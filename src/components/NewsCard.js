import React, { PureComponent } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { _ } from "lodash";
import he from "he"; // This is the encoder / decoder for the HTML special characters

import { selectNews } from "../actions/NewsActions";
import { connect } from "react-redux";

const styles = {
	viewStyle: {
		// backgroundColor: "#c33",
		padding: 10,
		flexDirection: "row",
		marginBottom: 5,
		marginTop: 5,
	},

	leftImageStyle: {
		width: 130,
		height: 100,
	},

	textViewStyle: {
		marginLeft: 10,
		flex: 1,
		flexWrap: "wrap",
	},

	titleStyle: {
		fontSize: 15,
        
	},

};

class NewsCard extends PureComponent {

	fetchSingleNews(newsID) {
		// console.log(this.props.navigationProp);
		this.props.selectNews(newsID, this.props.navigationProp);
	}

	checkForUndefined() {
		if(this.props.item._embedded["wp:featuredmedia"]) {
			return (
				<Image source={{uri: this.props.item._embedded["wp:featuredmedia"]["0"].source_url}}
						style={styles.leftImageStyle} />
			);
		}
			return(
				<Image source={{uri: "https://placehold.it/130x100"}}
						style={styles.leftImageStyle} />
			); 
		
	}

	render() {
		// console.log("Item Image: " + JSON.stringify(this.props.item._embedded["wp:featuredmedia"]["0"].source_url));
		return (
			<TouchableOpacity onPress={() => this.fetchSingleNews(this.props.item.id)}>
				<View style={styles.viewStyle}>
					{this.checkForUndefined()}
					<View style={styles.textViewStyle}>
						<Text style={styles.titleStyle}>{_.truncate(he.decode(this.props.item.title.rendered), { "length": 50 })}</Text>
						<Text>{_.truncate(he.decode(this.props.item.excerpt.rendered).replace(/<(?:.|\n)*?>/gm, ""), { "length": 100 })}</Text>
					</View>
				</View>
			</TouchableOpacity>
		);
	}
}
	


export default connect(null, {selectNews})(NewsCard);