import React, { Component } from "react";
import { View, Text , Dimensions, StyleSheet } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { ENTRIES1 } from "../static/Entries";
import SliderEntry from "./SliderEntry";

const SLIDER_1_FIRST_ITEM = 1;
const { width: viewportWidth } = Dimensions.get("window");

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

export const colors = {
    black: "#1a1917",
    gray: "#888888",
    background1: "#B721FF",
    background2: "#21D4FD",
};
const styles = StyleSheet.create({
    slider: {
        marginTop: 15,
        overflow: "visible", // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10, // for custom animation
    },
    paginationContainer: {
        paddingVertical: 8,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8,
    },
});
class MyCarousel extends Component {
    
    constructor (props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
        };
    }
    _renderItemWithParallax = ({item, index}, parallaxProps) => {
        // console.log(this.props.navigation);
        return (
            <SliderEntry
              navigationProp={this.props.navigation}
              data={item}
              even={(index + 1) % 2 === 0}
              parallax={true}
              parallaxProps={parallaxProps}
            />
        );
    }
    
    render() {
        const { items } = this.props;
        // console.log("My carou", items);
        return(
            <View>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={items}
                  renderItem={this._renderItemWithParallax}
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={SLIDER_1_FIRST_ITEM}
                  inactiveSlideScale={0.94}
                  inactiveSlideOpacity={0.7}
                  // inactiveSlideShift={20}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  loop={true}
                  loopClonesPerSide={2}
                  autoplay={true}
                  autoplayDelay={500}
                  autoplayInterval={3000}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                <Pagination
                  dotsLength={items.length}
                  activeDotIndex={this.state.slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor={"rgba(0, 0, 0, 0.92)"}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={colors.black}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }
}

export default MyCarousel;