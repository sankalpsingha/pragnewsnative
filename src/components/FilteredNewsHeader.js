import React from "react";
import { View, Text } from "react-native";

const styles = {
    textStyle: {
        fontSize: 20,
        marginTop: 20,
        marginLeft: 10,
        marginBottom: 10,
        color: "#2B2A2A",
        letterSpacing: 2,
    },
};

const FilteredNewsHeader = (props) => {
    return(
        <View>
            <Text style={styles.textStyle}>{props.headerText.toUpperCase()}</Text>
        </View>
    );
};

export default FilteredNewsHeader;