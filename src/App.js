import React from "react";
// import { View, Text } from 'react-native';
import { Provider } from "react-redux";
// Let us bring in the routers
import AppNavigator from "./Router";

import store from "./Store";
const App = () => {
	return (
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	);
};

export default App;
