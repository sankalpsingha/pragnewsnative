import {
    NEWS_FETCHED_SUCCESS,
    SINGLE_NEWS_FETCHED_SUCCESS,
    LOADING_START,
    MORE_NEWS_FETCHED,
    NEWS_ENTERTAINMENT_FETCHED_SUCCESS,
    NEWS_GEET_FETCHED_SUCCESS,
    NEWS_REGIONAL_FETCHED_SUCCESS,
    NEWS_NATIONAL_FETCHED_SUCCESS,
    NEWS_SPORTS_FETCHED_SUCCESS,
    NEWS_WORLD_FETCHED_SUCCESS,

} from "../actions/types";

const INITIAL_STATE = {
    news: [],
    loading: true,
    singleNews: {},
    entertainmentNews: [],
    geetNews: [],
    regionalNews: [],
    nationalNews: [],
    sportsNews: [],
    worldNews: [],
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NEWS_FETCHED_SUCCESS: 
            return { ...state, news: action.payload, loading: false };

        case SINGLE_NEWS_FETCHED_SUCCESS: 
            return { ...state, singleNews: action.payload, loading: false };
        
        case LOADING_START: 
            return { ...state, loading: true };

        case MORE_NEWS_FETCHED:
            return { ...state, ...action.payload };
        
        case NEWS_ENTERTAINMENT_FETCHED_SUCCESS: 
            return { ...state, entertainmentNews: action.payload, loading: false };
        
        case NEWS_GEET_FETCHED_SUCCESS: 
            return { ...state, geetNews: action.payload, loading: false };
            
        case NEWS_REGIONAL_FETCHED_SUCCESS: 
            return { ...state, regionalNews: action.payload, loading: false }; 
            
        case NEWS_NATIONAL_FETCHED_SUCCESS: 
            return { ...state, nationalNews: action.payload, loading: false }; 
          
        case NEWS_SPORTS_FETCHED_SUCCESS: 
            return { ...state, sportsNews: action.payload, loading: false }; 

        case NEWS_WORLD_FETCHED_SUCCESS: 
            return { ...state, worldNews: action.payload, loading: false }; 
            
        default: 
            return state;
    }
};