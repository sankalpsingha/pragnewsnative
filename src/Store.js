import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
import { createLogger } from "redux-logger";
import reducers from "./reducers";

const logger = createLogger();
const store = createStore(reducers, {}, compose(
	applyMiddleware(ReduxThunk, logger)

));

export default store;
