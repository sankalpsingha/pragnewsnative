import React, { Component } from "react";
import { Image, View, Platform, StyleSheet } from "react-native";


const styles = StyleSheet.create({
    viewStyle: {
        alignItems: "center", 
        justifyContent: "center", 
        flexDirection: "row", 
        flex: 1,
    },
    imgStyle: {
        width: 100, 
        height: 48, 
        marginRight: 50,
        ...Platform.select({
            ios: {
                width: 65, 
                height: 30, 
                marginRight: 10,
            },
            android: {
                width: 100, 
                height: 48, 
                marginRight: 50,
            },
          }),
    },
});
class HeaderLogo extends Component {
    render() {
        return (
                // eslint-disable-next-line react-native/no-inline-styles
            <View style={styles.viewStyle}>
            <Image 
                source={require("../images/praglogo.png")}
                // eslint-disable-next-line react-native/no-inline-styles
                style={styles.imgStyle}
            />
            </View>
        );
    }
}

export default HeaderLogo;