module.exports = {
    // "parser": "babel-eslint",

    "env": {
        "node": true,
        "es6": true,
        "react-native/react-native": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-native"
    ],
    "rules": {
        // "indent": [
        //     "error",
        //     "tab"
        // ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-mixed-spaces-and-tabs": 0,
        "react/jsx-uses-react": 2,
        "react/jsx-uses-vars" : 2,
        "react/react-in-jsx-scope": 2,
        "react-native/no-unused-styles": 2,
        "react-native/split-platform-components": 2,
        "react-native/no-inline-styles": 2,
        "react-native/no-color-literals": 2,
        "react-native/no-raw-text": 0,
        "arrow-body-style": 0,
        "global-require": 0,
        "no-underscore-dangle": 0,

         'accessor-pairs': 'off',

        // enforces return statements in callbacks of array's methods
        // http://eslint.org/docs/rules/array-callback-return
        'array-callback-return': 'error',

        // treat var statements as if they were block scoped
        'block-scoped-var': 'error',

        // require return statements to either always or never specify values
        'consistent-return': 'error',

        // specify curly brace conventions for all control statements
        curly: ['error', 'multi-line'],

        // require default case in switch statements
        'default-case': ['error', { commentPattern: '^no default$' }],

        // encourages use of dot notation whenever possible
        'dot-notation': ['error', { allowKeywords: true }],

        // enforces consistent newlines before or after dots
        // http://eslint.org/docs/rules/dot-location
        'dot-location': ['error', 'property'],

        // require the use of === and !==
        // http://eslint.org/docs/rules/eqeqeq
        eqeqeq: ['error', 'allow-null'],

        // make sure for-in loops have an if statement
        'guard-for-in': 'error',

        // disallow the use of alert, confirm, and prompt
        'no-alert': 'warn',

        // disallow use of arguments.caller or arguments.callee
        'no-caller': 'error',

        // disallow lexical declarations in case/default clauses
        // http://eslint.org/docs/rules/no-case-declarations.html
        'no-case-declarations': 'error',

        // disallow division operators explicitly at beginning of regular expression
        // http://eslint.org/docs/rules/no-div-regex
        'no-div-regex': 'off',

        // disallow else after a return in an if
        'no-else-return': 'error',

        // disallow empty functions, except for standalone funcs/arrows
        // http://eslint.org/docs/rules/no-empty-function
        'no-empty-function': ['error', {
        allow: [
            'arrowFunctions',
            'functions',
            'methods',
        ]
        }],

        // disallow empty destructuring patterns
        // http://eslint.org/docs/rules/no-empty-pattern
        'no-empty-pattern': 'error',

        // disallow comparisons to null without a type-checking operator
        'no-eq-null': 'off',

        // disallow use of eval()
        'no-eval': 'error',

        // disallow adding to native types
        'no-extend-native': 'error',

        // disallow unnecessary function binding
        'no-extra-bind': 'error',

        // disallow Unnecessary Labels
        // http://eslint.org/docs/rules/no-extra-label
        'no-extra-label': 'error',

        // disallow fallthrough of case statements
        'no-fallthrough': 'error',

        // disallow the use of leading or trailing decimal points in numeric literals
        'no-floating-decimal': 'error',

        // disallow reassignments of native objects or read-only globals
        // http://eslint.org/docs/rules/no-global-assign
        'no-global-assign': ['error', { exceptions: [] }],

        // disallow implicit type conversions
        // http://eslint.org/docs/rules/no-implicit-coercion
        'no-implicit-coercion': ['off', {
        boolean: false,
        number: true,
        string: true,
        allow: [],
        }],

        // disallow var and named functions in global scope
        // http://eslint.org/docs/rules/no-implicit-globals
        'no-implicit-globals': 'off',

        // disallow use of eval()-like methods
        'no-implied-eval': 'error',

        // disallow this keywords outside of classes or class-like objects
        'no-invalid-this': 'off',

        // disallow usage of __iterator__ property
        'no-iterator': 'error',

        // disallow use of labels for anything other then loops and switches
        'no-labels': ['error', { allowLoop: false, allowSwitch: false }],

        // disallow unnecessary nested blocks
        'no-lone-blocks': 'error',

        // disallow creation of functions within loops
        'no-loop-func': 'error',

        // disallow magic numbers
        // http://eslint.org/docs/rules/no-magic-numbers
        'no-magic-numbers': ['off', {
        ignore: [],
        ignoreArrayIndexes: true,
        enforceConst: true,
        detectObjects: false,
        }],

        // disallow use of multiple spaces
        'no-multi-spaces': 'error',

        // disallow use of multiline strings
        'no-multi-str': 'error',

        // disallow reassignments of native objects
        // TODO: deprecated in favor of no-global-assign
        'no-native-reassign': 'off',

        // disallow use of new operator when not part of the assignment or comparison
        'no-new': 'error',

        // disallow use of new operator for Function object
        'no-new-func': 'error',

        // disallows creating new instances of String, Number, and Boolean
        'no-new-wrappers': 'error',

        // disallow use of (old style) octal literals
        'no-octal': 'error',

        // disallow use of octal escape sequences in string literals, such as
        // var foo = 'Copyright \251';
        'no-octal-escape': 'error',

        // disallow reassignment of function parameters
        // disallow parameter object manipulation
        // rule: http://eslint.org/docs/rules/no-param-reassign.html
        'no-param-reassign': ['error', { props: true }],

        // disallow usage of __proto__ property
        'no-proto': 'error',

        // disallow declaring the same variable more then once
        'no-redeclare': 'error',

        // disallow use of assignment in return statement
        'no-return-assign': 'error',

        // disallow use of `javascript:` urls.
        'no-script-url': 'error',

        // disallow self assignment
        // http://eslint.org/docs/rules/no-self-assign
        'no-self-assign': 'error',

        // disallow comparisons where both sides are exactly the same
        'no-self-compare': 'error',

        // disallow use of comma operator
        'no-sequences': 'error',

        // restrict what can be thrown as an exception
        'no-throw-literal': 'error',

        // disallow unmodified conditions of loops
        // http://eslint.org/docs/rules/no-unmodified-loop-condition
        'no-unmodified-loop-condition': 'off',

        // disallow usage of expressions in statement position
        'no-unused-expressions': ['error', {
        allowShortCircuit: false,
        allowTernary: false,
        }],

        // disallow unused labels
        // http://eslint.org/docs/rules/no-unused-labels
        'no-unused-labels': 'error',

        // disallow unnecessary .call() and .apply()
        'no-useless-call': 'off',

        // disallow useless string concatenation
        // http://eslint.org/docs/rules/no-useless-concat
        'no-useless-concat': 'error',

        // disallow unnecessary string escaping
        // http://eslint.org/docs/rules/no-useless-escape
        'no-useless-escape': 'error',

        // disallow use of void operator
        // http://eslint.org/docs/rules/no-void
        'no-void': 'error',

        // disallow usage of configurable warning terms in comments: e.g. todo
        'no-warning-comments': ['off', { terms: ['todo', 'fixme', 'xxx'], location: 'start' }],

        // disallow use of the with statement
        'no-with': 'error',

        // require use of the second argument for parseInt()
        radix: 'error',

        // requires to declare all vars on top of their containing scope
        'vars-on-top': 'error',

        // require immediate function invocation to be wrapped in parentheses
        // http://eslint.org/docs/rules/wrap-iife.html
        'wrap-iife': ['error', 'outside'],

        // require or disallow Yoda conditions
        yoda: 'error',

        'comma-dangle': ['error', 'always-multiline'],

        // disallow assignment in conditional expressions
        'no-cond-assign': ['error', 'always'],

        // disallow use of console
        'no-console': 'warn',

        // disallow use of constant expressions in conditions
        'no-constant-condition': 'warn',

        // disallow control characters in regular expressions
        'no-control-regex': 'error',

        // disallow use of debugger
        'no-debugger': 'error',

        // disallow duplicate arguments in functions
        'no-dupe-args': 'error',

        // disallow duplicate keys when creating object literals
        'no-dupe-keys': 'error',

        // disallow a duplicate case label.
        'no-duplicate-case': 'error',

        // disallow empty statements
        'no-empty': 'error',

        // disallow the use of empty character classes in regular expressions
        'no-empty-character-class': 'error',

        // disallow assigning to the exception in a catch block
        'no-ex-assign': 'error',

        // disallow double-negation boolean casts in a boolean context
        // http://eslint.org/docs/rules/no-extra-boolean-cast
        'no-extra-boolean-cast': 'error',

        // disallow unnecessary parentheses
        // http://eslint.org/docs/rules/no-extra-parens
        'no-extra-parens': ['off', 'all', {
        conditionalAssign: true,
        nestedBinaryExpressions: false,
        returnAssign: false,
        }],

        // disallow unnecessary semicolons
        'no-extra-semi': 'error',

        // disallow overwriting functions written as function declarations
        'no-func-assign': 'error',

        // disallow function or variable declarations in nested blocks
        'no-inner-declarations': 'error',

        // disallow invalid regular expression strings in the RegExp constructor
        'no-invalid-regexp': 'error',

        // disallow irregular whitespace outside of strings and comments
        'no-irregular-whitespace': 'error',

        // disallow negation of the left operand of an in expression
        // TODO: deprecated in favor of no-unsafe-negation
        'no-negated-in-lhs': 'off',

        // disallow the use of object properties of the global object (Math and JSON) as functions
        'no-obj-calls': 'error',

        // disallow use of Object.prototypes builtins directly
        // http://eslint.org/docs/rules/no-prototype-builtins
        'no-prototype-builtins': 'error',

        // disallow multiple spaces in a regular expression literal
        'no-regex-spaces': 'error',

        // disallow sparse arrays
        'no-sparse-arrays': 'error',

        // Disallow template literal placeholder syntax in regular strings
        // http://eslint.org/docs/rules/no-template-curly-in-string
        // TODO: enable, semver-major
        'no-template-curly-in-string': 'off',

        // Avoid code that looks like two expressions but is actually one
        // http://eslint.org/docs/rules/no-unexpected-multiline
        'no-unexpected-multiline': 'error',

        // disallow unreachable statements after a return, throw, continue, or break statement
        'no-unreachable': 'error',

        // disallow return/throw/break/continue inside finally blocks
        // http://eslint.org/docs/rules/no-unsafe-finally
        'no-unsafe-finally': 'error',

        // disallow negating the left operand of relational operators
        // http://eslint.org/docs/rules/no-unsafe-negation
        'no-unsafe-negation': 'error',

        // disallow comparisons with the value NaN
        'use-isnan': 'error',

        // ensure JSDoc comments are valid
        // http://eslint.org/docs/rules/valid-jsdoc
        'valid-jsdoc': 'off',

        // ensure that the results of typeof are compared against a valid string
        'valid-typeof': 'error',

         // enforce or disallow variable initializations at definition
        'init-declarations': 'off',

        // disallow the catch clause parameter name being the same as a variable in the outer scope
        'no-catch-shadow': 'off',

        // disallow deletion of variables
        'no-delete-var': 'error',

        // disallow labels that share a name with a variable
        // http://eslint.org/docs/rules/no-label-var
        'no-label-var': 'error',

        // disallow specific globals
        'no-restricted-globals': 'off',

        // disallow declaration of variables already declared in the outer scope
        'no-shadow': 'error',

        // disallow shadowing of names such as arguments
        'no-shadow-restricted-names': 'error',

        // disallow use of undeclared variables unless mentioned in a /*global */ block
        'no-undef': 'error',

        // disallow use of undefined when initializing variables
        'no-undef-init': 'error',

        // disallow use of undefined variable
        // TODO: enable?
        'no-undefined': 'off',

        // disallow declaration of variables that are not used in the code
        'no-unused-vars': ['error', { vars: 'local', args: 'after-used' }],

        // disallow use of variables before they are defined
        'no-use-before-define': 'error'

    }
};